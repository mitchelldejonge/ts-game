/*
* @Author Mitch
*/
class Level {

    private name:string;
    private rooms:Room[];

    constructor(name:string, rooms:Room[]){

        this.name = name;
        this.rooms = rooms;

    }
    

    getRoomByCoords(x:number, y:number) : Room
    {
        for (let i=0;i<this.rooms.length;i++)
        {
            if (this.rooms[i].getX() == x && this.rooms[i].getY() == y)
            {
                return this.rooms[i];
            }
        }
        return this.rooms[0];
    }

    roomAtCoordsExists(x:number, y:number) : boolean
    {
        for (let i=0;i<this.rooms.length;i++)
        {
            if (this.rooms[i].getX() == x && this.rooms[i].getY() == y)
            {
                if (this.rooms[i].getRoomType() != "WALL")//if this isnt a blocked off room
                return true;
            }
        }
        return false;
    }

    getRooms() : Room[] {
        return this.rooms;
    }

    getName() : string {
        return this.name;
    }

}

