class Item {

    private itemId:number;
    private sprite:Sprite;

    constructor(id:number)
    {
        this.itemId = id;
        this.sprite = this.loadSprite();
    }

    getItemId() {
        return this.itemId;
    }

    getName() : string {
        
        switch(this.itemId)
        {
            case 1:
            return "Key";
            case 2:
            return "Big key";
            case 3:
            return "Map";
            case 4:
            return "Book";
            case 5:
            return "Spade"
            default:
                return "Unnamed Item (ID "+this.itemId+")";
        }
    }

    loadSprite() {

        switch (this.itemId) {
            case 1:
            return new Sprite("item/key.png");
            case 2:
            return new Sprite("item/bigkey.png");
            case 3:
            return new Sprite("item/map.png");
            case 4:
            return new Sprite("item/book.png");
            case 5:
            return new Sprite("item/spade.png");
            default:
            return null;
        }
    }

    getSprite() : Sprite {
        return this.sprite;
    }
}