class CollisionBox {

    private width: number;
    private height: number;
    private x: number;
    private y: number;

    constructor(x: number, y: number, width: number, height: number) {
        this.width = width;
        this.height = height;
        this.x = x;
        this.y = y;
    }

    public getX() {
        return this.x;
    }

    public getY() {
        return this.y;
    }

    public getWidth() {
        return this.width;
    }

    public getHeight() {
        return this.height;
    }
}