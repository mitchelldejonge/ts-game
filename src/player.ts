/*
* @Author Mitch
* This class represents a player.
*/
class Player {

    constructor(game:Game){
        this.game = game;
    }

    private game : Game;
    private currentLevel : Level;
    private currentRoom : Room;

    //movement
    private isMoving : boolean = false;
    private direction : number = 2;//face south
    private moveSpeed : number = 750;

    //inventory
    private inventory = new Inventory(this.game, 10);

    //location in the room
    private x : number = 100;
    private y : number = 100;

    //collision box
    public colBox = new CollisionBox(4, 36, 32, 8);

    //rendering
    private sprite : Sprite = new Sprite("player/playerStandD.png");

    //loop
    private intervalSetting = 2; // The desired interval in ms
    private previousLoopStart:number = 0; //The time the previous iteration started
    private intervalSeconds:number = 0; // The calculated interval between the start of the last iteration and the current one

    getCurrentRoom() : Room {
        return this.currentRoom;
    }

    setCurrentLevel(level:Level) {
        this.currentLevel = level;
    }

    getCurrentLevel() : Level {
        return this.currentLevel;
    }

    getMoveSpeed() : number {
        return this.moveSpeed;
    }

    setMoveSpeed(s:number) {
        this.moveSpeed = s;
    }

    getX() {
        return this.x;
    }

    getY() {
        return this.y;
    }

    getColBox() {
        return this.colBox;
    }

    teleportTo(room:Room) : void
    {
        this.currentRoom = room;
        console.log("You move to a new room: ("+this.currentRoom.getX()+", "+this.currentRoom.getY()+"), roomType: "+this.currentRoom.getRoomType());
    }

    moveInDirection(dir:number) : boolean
    {

        let currentX = this.getCurrentRoom().getX();
        let currentY = this.getCurrentRoom().getY();

        let xModifier = 0;
        let yModifier = 0;

        switch(dir)
        {
            case 0://north
                yModifier--;
            break;

            case 1://east
                xModifier++;
            break;

            case 2://south
                yModifier++;
            break;

            case 3://west
                xModifier--;
            break;
        }

        let newX = currentX+xModifier;
        let newY = currentY+yModifier;

        //check if the room we want to go to actually exists
        if (!this.getCurrentLevel().roomAtCoordsExists(newX, newY))
        {
            console.log("Tried to move to a room that doesn't exist: ("+newX+", "+newY+").")
            return false;
        }

        let newRoom = this.getCurrentLevel().getRoomByCoords(newX, newY);

        this.teleportTo(newRoom);
        console.log("You move to a new room: ("+newX+", "+newY+"), roomType: "+newRoom.getRoomType());
        this.game.getRenderEngine().update();
    }

    getDirection() {
       return this.direction;
    }

    setLocation(x:number, y:number) {
        this.x = x;
        this.y = y;
    }

    startMoving(dir:number)
    {
        if (this.direction == dir && this.isMoving)
        return;
        this.direction = dir;
        this.isMoving = true;
        this.setSprite(this.getWalkSprite(dir));
        this.movementLoop(dir);
    }

    stopMoving()
    {
        if (!this.isMoving)
        return;
        this.isMoving = false;
        this.setSprite(this.getStandSprite(this.getDirection()));
    }

    private movementLoop(dir:number) : void {

        //loop calculation
        var loopStart = window.performance.now();

        if (this.previousLoopStart == 0) {
            this.intervalSeconds = this.intervalSetting / 1000;
        } else {
            this.intervalSeconds = (loopStart - this.previousLoopStart) / 1000;
        }

        //moving
        if (!this.isMoving){
            this.setSprite(this.getStandSprite(this.getDirection()));
            return;
        }

        if (dir != this.getDirection())
        return;

        let col = this.game.getCollision();
        let displacement = Math.round(this.getMoveSpeed() * this.intervalSeconds);
        let roomWidth = 896;
        let roomHeight = 525;
        let wallLength = 47;
        let cB = this.getColBox();

        switch(this.getDirection())
        {
            case 0://north
            if (!col.collides(this.getX()+cB.getX(), this.getY()+cB.getY()-displacement, cB.getWidth(), cB.getHeight(),0,0,roomWidth,wallLength))//wall collision
                this.setLocation(this.getX(), this.getY()-displacement);
            break;
            case 1://east
            if (!col.collides(this.getX()+cB.getX()+displacement, this.getY()+cB.getY(), cB.getWidth(), cB.getHeight(), roomWidth-wallLength, 0, wallLength, roomHeight))//wall collision
                this.setLocation(this.getX()+displacement, this.getY());
            break;
            case 2://south
            if (!col.collides(this.getX()+cB.getX(), this.getY()+cB.getY()+displacement, cB.getWidth(), cB.getHeight(),0,roomHeight-wallLength,roomWidth,wallLength))//wall collision
                this.setLocation(this.getX(), this.getY()+displacement);
            break;
            case 3://west
            if (!col.collides(this.getX()+cB.getX()-displacement, this.getY()+cB.getY(), cB.getWidth(), cB.getHeight(),0,0,wallLength,roomHeight))//wall collision
                this.setLocation(this.getX()-displacement, this.getY());
            break;
        }

        this.game.getRenderEngine().update();

        var diff = (window.performance.now() - loopStart);
        setTimeout(()=>{this.movementLoop(dir)},this.intervalSetting-diff);
    }

    getStandSprite(dir:number) : Sprite {
        switch (dir)
        {
            case 0://north
            return this.game.getSprites().standU;
            case 1://east
            return this.game.getSprites().standR;
            case 2://south
            return this.game.getSprites().standD;
            case 3://west
            return this.game.getSprites().standL;
        }
    }

    getWalkSprite(dir:number) {
        switch (dir)
        {
            case 0://north
            return this.game.getSprites().walkU;
            case 1://east
            return this.game.getSprites().walkR;
            case 2://south
            return this.game.getSprites().walkD;
            case 3://west
            return this.game.getSprites().walkL;
        }
    }

    getSprite(): Sprite {

        return this.sprite;
    }

    setSprite(s:any) {
        this.sprite = s;
        this.game.getRenderEngine().update();
    }

    getInventory() {
        return this.inventory;
    }

    distanceToPoint(x:number,y:number,x2:number,y2:number) {
        var a = x - x2;
        var b = y - y2;
        var c = Math.sqrt( a*a + b*b );

        return c;
    }

    getCurrentTooltip() : Tooltip {
        let x = this.getX();
        let y = this.getY();
        let col = this.game.getCollision();

        let currentRoom = this.game.getPlayer().getCurrentRoom();
        let w = 896;
        let h = 525;
        let wallLength = 47;
        let doorLength = 32;

        //if close to east door
        if(x > 780 && y >= 220 && y <= 280) {
            if (this.currentLevel.roomAtCoordsExists(currentRoom.getX()+1,currentRoom.getY()) || currentRoom.getDoor(1).getIsExit()){
                if (currentRoom.getDoor(1).getIsExit()) {
                    return new Tooltip("F", "End-game exit");
                }
                if (currentRoom.getDoor(1).isLocked()) {
                    return new Tooltip("F", "Unlock locked door");
                } else {
                    return new Tooltip("F", "Enter doorway");
                }
            }
        }

        //if close to west door
        if(x <= 75 && y >= 220 && y <= 280) {
            if (this.currentLevel.roomAtCoordsExists(currentRoom.getX()-1,currentRoom.getY()) || currentRoom.getDoor(3).getIsExit()){
                if (currentRoom.getDoor(3).getIsExit()) {
                    return new Tooltip("F", "End-game exit");
                }

                if (currentRoom.getDoor(3).isLocked()) {
                    return new Tooltip("F", "Unlock locked door");
                } else {
                    return new Tooltip("F", "Enter doorway");
                }
            }
        }

        //if close to south door
        if(y >= 420 && x >= 400 && x <= 460) {
            if (this.currentLevel.roomAtCoordsExists(currentRoom.getX(),currentRoom.getY()+1) || currentRoom.getDoor(2).getIsExit()){
                if (currentRoom.getDoor(2).getIsExit()) {
                    return new Tooltip("F", "End-game exit");
                }
                if (currentRoom.getDoor(2).isLocked()) {
                    return new Tooltip("F", "Unlock locked door");
                } else {
                    return new Tooltip("F", "Enter doorway");
                }
            }
        }

        //if close to north door
        if(y <= 50 && x >= 400 && x <= 460) {
            if (this.currentLevel.roomAtCoordsExists(currentRoom.getX(),currentRoom.getY()-1) || currentRoom.getDoor(0).getIsExit()){
                if (currentRoom.getDoor(0).getIsExit()) {
                    return new Tooltip("F", "End-game exit");
                }
                if (currentRoom.getDoor(0).isLocked()) {
                    return new Tooltip("F", "Unlock locked door");
                } else {
                    return new Tooltip("F", "Enter doorway");
                }
            }
        }

        let iD =  this.getCurrentRoom().getItemDrops();

        for (let i = 0;i<iD.length;i++)
        {
            let itemColBox = new CollisionBox(iD[i].getX(), iD[i].getY(), iD[i].getSprite().get().width, iD[i].getSprite().get().height)
            if (this.game.getCollision().boxesCollide(this.getColBox(), this.getX(), this.getY(), itemColBox, 0, 0))
            {
                return new Tooltip("F", "Pickup "+iD[i].getName()); 
            }
        }

        return new Tooltip("","");
    }

    unlockDoor(door:Door) {
        if (this.getInventory().contains(door.getItemRequirement())) {
            door.setLocked(false);
            this.game.getOppositeDoor(door).setLocked(false);
            this.getInventory().deleteItem(door.getItemRequirement());
            this.game.getRenderEngine().update();
        }
    }


}