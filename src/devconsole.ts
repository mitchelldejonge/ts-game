/*
* @Author Mitch
*/
class DevConsole {

    private game:Game;
    private isOn:boolean=false;
    private input:string="";

    constructor(game:Game) {
        this.game = game;
    }

    setEnabled(o:boolean)
    {
        this.isOn = o;
    }

    isEnabled() : boolean
    {
        return this.isOn;
    }

    addInput(s:string) {
        this.input+=s;
    }

    setInput(s:string) {
        this.input = s;
    }

    runCommand(input:string):void
    {
        let args = input.split(" ");
        let command = args[0].toLowerCase();

        switch (command)
        {
            case "teleport":
                let x = parseInt(args[1]);
                let y = parseInt(args[2]);

                if (!this.game.getPlayer().getCurrentLevel().roomAtCoordsExists(x,y))
                break;

                this.game.getPlayer().teleportTo(this.game.getPlayer().getCurrentLevel().getRoomByCoords(x,y));
            break;

            case "setspeed":
            let speed = parseInt(args[1])

            this.game.getPlayer().setMoveSpeed(speed);
            break;

            case "item":
            this.game.getPlayer().getInventory().addItem(parseInt(args[1]));
            break; 
            case "clear":
            this.game.getPlayer().getInventory().clear();
            break;

            case "pos":
            console.log(this.game.getPlayer().getX());
            console.log(this.game.getPlayer().getY());
            break;

            case "dump": //dump the current state of the game instance into the browser console
            console.log(this.game);
            break;

        }

        this.input = "";
    }

    getInput() {
        return this.input;
    }
}