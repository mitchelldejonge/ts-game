/* @Author Mitch*/
//An inventory contains items, which can be added and deleted.

class Inventory {

    private game:Game;
    private size:number;
    private items:Array<Item>;

    constructor(game:Game, size:number) {
        this.game = game;
        this.size = size;
        this.init();
    }

    init() {

        this.items = new Array(this.size);

        for (let i=0;i<this.size;i++)
        {
            this.items[i] = new Item(0);
        }
    }

    addItem(id:number):boolean {

        for (let i=0;i<this.size;i++)
        {
            if (this.items[i].getItemId() == 0)
            {
                this.items[i] = new Item(id);
                return true;
            }
        }
        return false;
    }

    deleteItem(id:number) {
        for (let i=0;i<this.size;i++)
        {
            if (this.items[i].getItemId() == id)
            {
                this.items[i] = new Item(0);
                return;
            }
        }
    }

    getFreeSlots() {

        let a=0;

        for (let i=0;i<this.size;i++)
        {
            if(this.items[i].getItemId() == 0)
            {
                a++;
            }
        }
        return a;
    }

    getFirstFreeSlot() {

        for (let i=0;i<this.size;i++)
        {
            if(this.items[i].getItemId() == 0)
            {
                return i;
            }
        }
    }

    contains(itemId:number) : boolean {

        for (let i=0;i<this.size;i++)
        {
            if(this.items[i].getItemId() == itemId)
            {
                return true;
            }
        }
        return false;
    }

    clear() {
        for (let i=0;i<this.size;i++)
        {
            this.items[i] = new Item(0);
        }
    }

    getSize() {
        return this.size;
    }

    getItems() {
        return this.items;
    }

}