/*
* @Author Mitch
*/
class InputHandler {
    
    constructor(game:Game) {
        this.game = game;
    }

    private game:Game;

    handleKeyPress(char:string){

        if (this.game.getDevConsole().isEnabled() && !this.game.getEventHandler().isSpecialKey(parseInt(char))) {
        this.game.getDevConsole().addInput(char);
        this.game.getRenderEngine().update();
        return;
        }

        switch(char)
        {
            /* testing tool. Moving with numpad arrows between rooms */
            case "W":
                //this.game.getPlayer().moveInDirection(0);
                this.game.getPlayer().startMoving(0);
            break;

            case "A":
                //this.game.getPlayer().moveInDirection(3);
                this.game.getPlayer().startMoving(3);
            break;
            
            case "S":
                //this.game.getPlayer().moveInDirection(2);
                this.game.getPlayer().startMoving(2);
            break;
            
            case "D":
                //this.game.getPlayer().moveInDirection(1);
                this.game.getPlayer().startMoving(1);
            break;

            case "F":
                this.game.getInteraction().interact();
            break;

            case "192"://Grave key
                if (this.game.getDevConsole().isEnabled())
                this.game.getDevConsole().setEnabled(false);
                else
                this.game.getDevConsole().setEnabled(true);

                this.game.getRenderEngine().update();                
            break;

            case "8"://backspace
                if (!this.game.getDevConsole().isEnabled())
                return;
                //remove one char from input
                this.game.getDevConsole().setInput(this.game.getDevConsole().getInput().substr(0,this.game.getDevConsole().getInput().length-1));
                this.game.getRenderEngine().update();
            break;

            case "13":
            if (this.game.getDevConsole().isEnabled())
            this.game.getDevConsole().runCommand(this.game.getDevConsole().getInput());
            this.game.getRenderEngine().update();
            break;
        }
    }

    handleKeyRelease(char:string){


        switch(char)
        {
            case "W":
                if (this.game.getPlayer().getDirection() == 0)
                this.game.getPlayer().stopMoving();
            break;
            case "A":
                if (this.game.getPlayer().getDirection() == 3)
                this.game.getPlayer().stopMoving();
            break;
            case "S":
                if (this.game.getPlayer().getDirection() == 2)
                this.game.getPlayer().stopMoving();
            break;
            case "D":
                if (this.game.getPlayer().getDirection() == 1)
                this.game.getPlayer().stopMoving();
            break;
        }
    }
}