class ItemDrop extends Item {
    
    //location in room
    private x:number=0;
    private y:number=0;
    

    public getX() {
        return this.x;
    }

    public getY() {
        return this.y;
    }

    public setX(n:number) {
        this.x = n;
    }

    public setY(n:number) {
        this.y = n;
    }

}