/*
* @Author Mitch
*/
class RenderEngine {

    private game:Game;
    private gameCanvas:HTMLCanvasElement;
    private crc:CanvasRenderingContext2D;

    constructor(game:Game, gameCanvas:HTMLCanvasElement)
    {
        this.gameCanvas = gameCanvas;
        this.game = game;
        this.crc = this.gameCanvas.getContext("2d");
    }

    clearCanvas() {
        this.crc.clearRect(0,0,1280,720);
    }

    drawBackground() {
        this.crc.fillStyle = "black";
        this.crc.fillRect(0,0,1280,720); 
    }

    update()
    {
        let player = this.game.getPlayer();

        //refresh canvas
        this.clearCanvas();
        
        //draw the floor
        this.drawSprite(player.getCurrentRoom().getFloorSprite(),0,0);

        //draw the walls
        this.drawSprite(player.getCurrentRoom().getWallSprite(),0,0);

        //draw doors
        this.drawDoors();

        //draw item drops on floor
        let d = player.getCurrentRoom().getItemDrops();
        if (d.length != 0) {
            for(let i=0;i<d.length;i++)
            {
                this.drawSprite(d[i].getSprite(), d[i].getX(), d[i].getY());
            }
        }

        //render player
        this.renderPlayer();

        this.drawTooltip(this.game.getPlayer().getCurrentTooltip());


        //draw player inventory
       this.drawInventory(player.getInventory());

       //draw castle bg if we won the game
       if (this.game.ended) {
        this.drawSprite(this.game.getSprites().bgCastle, 0 , 0);
        let s = "You found the exit! Congratulations!";
        this.crc.fillStyle = "black";
        let f = this.crc.font;
        this.crc.font = "italic small-caps bold 30px arial";
        this.drawText("You found the exit! Congratulations!",(896/2)-(this.crc.measureText(s).width/2),525/2)
        this.crc.font = f;
       }


        //draw dev console
        if (this.game.getDevConsole().isEnabled())
        this.drawDevConsole(this.game.getDevConsole());
    }
    
    drawText(s:string, x:number, y:number) {
        this.crc.fillText(s, x, y);
    }

    renderPlayer() {
        let player = this.game.getPlayer();
        this.drawSprite(player.getSprite(), player.getX(), player.getY());
    }

    drawSprite(s:any, x:number, y:number) {
        if (s instanceof Sprite) {
            this.crc.drawImage(s.get(),x,y);
        } else if (s instanceof AnimatedSprite) {
            this.crc.drawImage(s.getFrame(s.getFrameIndex()), x, y);
        }
    }

    drawDevConsole(d:DevConsole)
    {
        this.crc.fillStyle = "black";
        this.crc.fillRect(0, 0, 896, 15);
        this.crc.fillStyle = "white";
        this.drawText("DevConsole > "+d.getInput(),10,10);
    }

    drawInventory(inv:Inventory) {
        let x=(this.gameCanvas.width/2)-((inv.getSize()*64)/2);
        let y=this.gameCanvas.height-66;
        for(let i=0;i<inv.getSize();i++)
        {
            this.crc.fillStyle = "black";
            this.crc.fillRect(x,y+0,2,64);
            this.crc.fillRect(x+64,y+0,2,64);
            this.crc.fillRect(x,y+0,64,2);
            this.crc.fillRect(x,y+64,64,2);
            this.drawSprite(inv.getItems()[i].getSprite(), x+2,y+2);
            x+=64;
        }
    }

    drawDoors() {

        let currentRoom = this.game.getPlayer().getCurrentRoom();
        let w = 896;;
        let h = 525;
        let wallLength = 47;
        let doorLength = 32;

        if (this.game.getPlayer().getCurrentLevel().roomAtCoordsExists(currentRoom.getX()+1, currentRoom.getY()) || currentRoom.getDoor(1).getIsExit()) {
            if (currentRoom.getDoor(1).isLocked()) {
                this.drawSprite(this.game.getSprites().doorLockedR,w-wallLength,(h/2)-(doorLength/2));
            } else {
                this.drawSprite(this.game.getSprites().doorOpenR,w-wallLength,(h/2)-(doorLength/2));
            }
        }
        if (this.game.getPlayer().getCurrentLevel().roomAtCoordsExists(currentRoom.getX()-1, currentRoom.getY()) || currentRoom.getDoor(3).getIsExit()) {
            if (currentRoom.getDoor(3).isLocked()) {
                this.drawSprite(this.game.getSprites().doorLockedL,doorLength/2,(h/2)-(doorLength/2));
            } else {
                this.drawSprite(this.game.getSprites().doorOpenL,doorLength/2,(h/2)-(doorLength/2));
            }
        }
        if (this.game.getPlayer().getCurrentLevel().roomAtCoordsExists(currentRoom.getX(), currentRoom.getY()+1) || currentRoom.getDoor(2).getIsExit()) {
            if (currentRoom.getDoor(2).isLocked()) {
            this.drawSprite(this.game.getSprites().doorLockedD,(w/2)-doorLength,h-wallLength);
                } else {
            this.drawSprite(this.game.getSprites().doorOpenD,(w/2)-doorLength,h-wallLength);
                }
            }

        if (this.game.getPlayer().getCurrentLevel().roomAtCoordsExists(currentRoom.getX(), currentRoom.getY()-1) || currentRoom.getDoor(0).getIsExit()) {
            if (currentRoom.getDoor(0).isLocked()) {
                this.drawSprite(this.game.getSprites().doorLockedU,(w/2)-doorLength,doorLength/2);
            } else {
                this.drawSprite(this.game.getSprites().doorOpenU,(w/2)-doorLength,doorLength/2);                
            }
        }

    }

    drawTooltip(tt:Tooltip)
    {
        if(tt.getText() == "" || tt.getAction() == "")
        return;

        let t = this.crc.measureText(tt.getText());
        let a = this.crc.measureText(tt.getAction());
        let w = t.width+a.width+15;
        let h = 13;
        let f = this.crc.font;
        let pWidth = 32;
        let yAdd = 13;

        this.crc.fillStyle = "black";
        this.crc.fillRect(this.game.getPlayer().getX()-(w/2)+(pWidth/2),this.game.getPlayer().getY()-yAdd,w,h);
        this.crc.fillStyle = "yellow";
        this.crc.font = "bold "+f;
        this.drawText(tt.getAction(),this.game.getPlayer().getX()+5-(w/2)+(pWidth/2),this.game.getPlayer().getY()-yAdd+(h/2)+2);
        this.crc.font = f;
        this.crc.fillStyle = "white";
        this.drawText(tt.getText(),this.game.getPlayer().getX()-(w/2)+(pWidth/2)+10+a.width,this.game.getPlayer().getY()-yAdd+(h/2)+2);
    }
    
}