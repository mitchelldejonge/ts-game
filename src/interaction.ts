class Interaction {

    private game:Game;

    //coordinates to teleport the player to the right place when interacting with a door.

    private rightX:number = 811;
    private leftX:number = 46;
    private midY:number = 253;

    private topY:number = 13;
    private bottomY:number = 433;
    private midX:number = 430;

    //

    constructor(game:Game) {
        this.game = game;
    }

    public interact() : boolean {

        let p = this.game.getPlayer();
        let x = p.getX();
        let y = p.getY();
        let col = this.game.getCollision();
        let currentRoom = this.game.getPlayer().getCurrentRoom();
        let w = 896;
        let h = 525;
        let wallLength = 47;
        let doorLength = 32;

        //interact east door
        if(x > 780 && y >= 220 && y <= 280) {
                if (currentRoom.getDoor(1).getIsExit())
                    this.game.ended = true;
            if (p.getCurrentLevel().roomAtCoordsExists(currentRoom.getX()+1,currentRoom.getY())){
                if (currentRoom.getDoor(1).isLocked()) {
                    p.unlockDoor(currentRoom.getDoor(1));
                } else {
                    p.moveInDirection(1);
                    p.setLocation(this.leftX,this.midY);
                    this.game.getRenderEngine().update();
                    return true;
                }
            }
        }

        //interact west door
        if(x <= 75 && y >= 220 && y <= 280) {
                if (currentRoom.getDoor(3).getIsExit())
                    this.game.ended = true;
            if (p.getCurrentLevel().roomAtCoordsExists(currentRoom.getX()-1,currentRoom.getY())){
                if (currentRoom.getDoor(3).isLocked()) {
                    p.unlockDoor(currentRoom.getDoor(3));
                    return true;
                } else {
                    p.moveInDirection(3);
                    p.setLocation(this.rightX,this.midY);
                    this.game.getRenderEngine().update();
                    return true;
                }
            }
        }

        //interact south door
        if(y >= 420 && x >= 400 && x <= 460) {
                if (currentRoom.getDoor(2).getIsExit())
                    this.game.ended = true;
            if (p.getCurrentLevel().roomAtCoordsExists(currentRoom.getX(),currentRoom.getY()+1)){
                if (currentRoom.getDoor(2).isLocked()) {
                    p.unlockDoor(currentRoom.getDoor(2));
                    return true;
                } else {
                    p.moveInDirection(2);
                    p.setLocation(this.midX,this.topY);
                    this.game.getRenderEngine().update();
                    return true;
                }
            }
        }

        //interact north door
        if(y <= 50 && x >= 400 && x <= 460) {
                if (currentRoom.getDoor(0).getIsExit())
                    this.game.ended = true;
            if (p.getCurrentLevel().roomAtCoordsExists(currentRoom.getX(),currentRoom.getY()-1)){
                if (currentRoom.getDoor(0).isLocked()) {
                    p.unlockDoor(currentRoom.getDoor(0));
                    return true;
                } else {
                    p.moveInDirection(0);
                    p.setLocation(this.midX,this.bottomY);
                    this.game.getRenderEngine().update();
                    return true;
                }
            }
        }

        let iD =  p.getCurrentRoom().getItemDrops();
        //interact with items on the floor
        for (let i = 0;i<iD.length;i++)
        {
            let itemColBox = new CollisionBox(iD[i].getX(), iD[i].getY(), iD[i].getSprite().get().width, iD[i].getSprite().get().height);
            
            if (this.game.getCollision().boxesCollide(p.getColBox(), p.getX(), p.getY(), itemColBox, 0, 0))
            {
                if (p.getInventory().addItem(iD[i].getItemId()) == true) {
                iD.splice(i, 1);
                this.game.getRenderEngine().update();
                } else {
                    //no space
                }
            }
        }

        return false;
    }
}