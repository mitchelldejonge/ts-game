class Collision {
    
    private game:Game;
    constructor(game:Game) {
        this.game = game;
    }

    public collides(x1:number,y1:number,w1:number,h1:number,x2:number,y2:number,w2:number,h2:number) : boolean {
        
        let left1=x1;
        let right1=x1+w1;
        let top1=y1;
        let bottom1=y1+h1;

        let left2=x2;
        let right2=x2+w2;
        let top2=y2;
        let bottom2=y2+h2;

        return !(left2 > right1 || 
        right2 < left1 || 
        top2 > bottom1 ||
        bottom2 < top1);
    }

    public boxesCollide(c1: CollisionBox, x1: number, y1: number, c2: CollisionBox, x2: number, y2: number): boolean {
        return this.collides(c1.getX()+x1, c1.getY()+y1, c1.getWidth(), c1.getHeight(), c2.getX()+x2, c2.getY()+y2, c2.getWidth(), c2.getHeight());
    }
}