/*
* @Author Mitch
* The Sprite Storage loads all images and saves the instances so that they are all pre-loaded.
*/
class SpriteStorage {

    /*Player sprites*/
    public walkR = new AnimatedSprite("player/walk/walkR", 40);
    public walkL = new AnimatedSprite("player/walk/walkL", 40);
    public walkU = new AnimatedSprite("player/walk/walkU", 40);
    public walkD = new AnimatedSprite("player/walk/walkD", 40);

    public standR = new Sprite("player/playerStandR.png");
    public standL = new Sprite("player/playerStandL.png");
    public standU = new Sprite("player/playerStandU.png");
    public standD = new Sprite("player/playerStandD.png");

    /*Room sprites */
    public doorOpenR = new Sprite("rooms/door/openR.png");
    public doorOpenL = new Sprite("rooms/door/openL.png");
    public doorOpenU = new Sprite("rooms/door/openU.png");
    public doorOpenD = new Sprite("rooms/door/openD.png");

    public doorLockedR = new Sprite("rooms/door/lockedR.png");
    public doorLockedL = new Sprite("rooms/door/lockedL.png");
    public doorLockedU = new Sprite("rooms/door/lockedU.png");
    public doorLockedD = new Sprite("rooms/door/lockedD.png");

    /*backgrounds*/

    public bgCastle = new Sprite ("bg/castle.png");

}