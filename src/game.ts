/*
* @Author Mitch
*/

class Game {

    constructor(){
        this.eventHandler = new EventHandler(this);
        let gameCanvas:HTMLCanvasElement = <HTMLCanvasElement>document.getElementById("gameCanvas");
        this.renderEngine = new RenderEngine(this, gameCanvas);
        this.devConsole = new DevConsole(this);
        this.spriteStorage = new SpriteStorage();
        this.collisionHandler = new Collision(this);
        this.interaction = new Interaction(this);
        this.startGame();
    }

    private title : string = "ts-game";
    private player : Player = new Player(this);
    private eventHandler : EventHandler;
    private renderEngine : RenderEngine;
    private devConsole : DevConsole;
    private spriteStorage : SpriteStorage;
    private collisionHandler : Collision;
    private interaction : Interaction;
    public ended : boolean = false;

    generateTestLevel(xlength:number, ylength:number) : Level
    {
        let i = 0;
        let locs : Room[] = new Array(xlength*ylength);
        for (let x=0;x<xlength;x++)
        {
            for (let y=0;y<ylength;y++)
            {    
                locs[i] = new Room(x, y, "TEST_ROOM");
                i++;
            }
        }
        
        let level = new Level("Testing level", locs);

        //setting unwanted rooms to wall type
        level.getRoomByCoords(1, 1).setRoomType("WALL");

        //locking doors
        level.getRoomByCoords(0, 0).getDoor(2).setLocked(true);
        level.getRoomByCoords(0, 2).getDoor(1).setLocked(true);

        //adding item drops
        let key = new ItemDrop(1);
        let book = new ItemDrop(4);
        key.setX(300);
        key.setY(400);
        level.getRoomByCoords(0, 1).getItemDrops().push(key);
        key.setX(600);
        key.setY(80);
        level.getRoomByCoords(1, 0).getItemDrops().push(key);
        book.setX(500);
        book.setY(250);
        level.getRoomByCoords(0, 0).getItemDrops().push(book);

        //setting exit
        level.getRoomByCoords(1, 2).getDoor(0).setIsExit(true);

        return level;
    }

    getPlayer() : Player {
        return this.player;
    }

    getEventHandler(): EventHandler {
        return this.eventHandler;
    }

    getRenderEngine() {
        return this.renderEngine;
    }

    startGame() : void {
        let level = this.generateTestLevel(2,3);
        this.getPlayer().setCurrentLevel(level);
        this.getPlayer().teleportTo(level.getRoomByCoords(0,0));
        console.log("Welcome to "+level.getName()+", you are currently in this room: ");
        console.log(this.getPlayer().getCurrentRoom());
    }

    getDevConsole() : DevConsole {
        return this.devConsole;
    }

    getSprites() {
        return this.spriteStorage;
    }

    getCollision() {
        return this.collisionHandler;
    }

    getInteraction() {
        return this.interaction;
    }

    getOppositeDoor(door:Door) {
        let oppositeDoor:Door;    
        let xdiff = 0;
        let ydiff = 0;
        let oppSide = 0;
        switch (door.getDirection())
        {
            case 0:
            ydiff--;
            oppSide = 2;
            break;
            case 1:
            xdiff++;
            oppSide = 3;
            break;
            case 2:
            ydiff++;
            oppSide = 0;
            break;
            case 3:
            xdiff--;
            oppSide = 1;
            break;
        }
        let oRoom = this.getPlayer().getCurrentLevel().getRoomByCoords(door.getRoom().getX()+xdiff,door.getRoom().getY()+ydiff);
        return oRoom.getDoor(oppSide);
    }
}