/*
* @Author Mitch
*/
class EventHandler {

    constructor(game:Game){
        this.addEventHandlers();
        this.game = game;
        this.inputHandler = new InputHandler(game);
    }

    private inputHandler:InputHandler;
    private game:Game;
    private special_keys:number[] = [192, 8, 13];
    
        addEventHandlers() {
            document.addEventListener('keydown', this.keyboardInput);
            document.addEventListener('keyup', this.keyboardRelease);
        }

        keyboardInput = (event: KeyboardEvent):void => {

                    if (this.isSpecialKey(event.keyCode)) {
                    this.getInputHandler().handleKeyPress(event.keyCode.toString());
                    return;
                    }


                this.getInputHandler().handleKeyPress(String.fromCharCode(event.keyCode));
                
        }

        keyboardRelease = (event: KeyboardEvent):void => {
                this.getInputHandler().handleKeyRelease(String.fromCharCode(event.keyCode));
        }

        getInputHandler() : InputHandler {
            return this.inputHandler;
        }

        isSpecialKey(n:number) :boolean {
            for (let i = 0;i<this.special_keys.length;i++) {
                if (this.special_keys[i] == n) {
                    return true;
                }
            }
            return false;
        }
}