/* @Author Mitch */
/*
    A tooltip will be displayed above the players head as a hint.
*/
class Tooltip {

    private action:string;
    private text:string;

    constructor(a:string, t:string) {
        this.action = a;
        this.text = t;
    }

    getText() {
        return this.text;
    }

    getAction() {
        return this.action;
    }

}