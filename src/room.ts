/*
* This class represents a room, which is a location in a level.
*/

class Room {

    private x : number;
    private y : number;
    private roomType : string;
    private doors: Door[];
    private itemDrops : ItemDrop[];

    constructor(x:number, y:number, roomType:string){
        this.x = x;
        this.y = y;
        this.roomType = roomType;
        this.doors = new Array(4);
        this.itemDrops = new Array(0);
        let d = new ItemDrop(Math.round(Math.random()*4)+1);
        for (let i=0;i<4;i++){
            this.doors[i] = new Door(this);
            this.doors[i].setDirection(i);
        }
    }

    public getX() : number {
        return this.x;
    }

    public getY() : number {
        return this.y;
    }

    public getRoomType() : string {
        return this.roomType;
    }

    public setRoomType(s:string) {
        this.roomType = s;
    }

    public getDoor(dir:number) : Door {
        return this.doors[dir];
    }

    public getItemDrops() : ItemDrop[] {
        return this.itemDrops;
    }

    public getFloorSprite() : Sprite {
        switch (this.getRoomType())
        {
            case "TEST_ROOM":
            return new Sprite("rooms/floor/darkStone.png")
        }
    }

    public getWallSprite() : Sprite {
        switch (this.getRoomType())
        {
            case "TEST_ROOM":
            return new Sprite("rooms/wall/stoneWall.png")
        } 
    }

}
