/*
* @Author Mitch
*/
class AnimatedSprite extends Sprite  {

    private imgs:HTMLImageElement[];
    private frameIndex:number = 0;
    private animationSpeed:number;
    
    constructor(src:string, speed:number) {
        super(src);        
        this.src= "assets/sprites/"+src;
        this.initImg();
        this.animationSpeed = speed;
        this.loopFrames(this.animationSpeed);
    }

    getFrame(index:number) {
        return this.imgs[index];
    }

    getFrameIndex() {
        return this.frameIndex;
    }

    loopFrames(speed:number) {
        if (this.getAnimationLength() == this.frameIndex)
        {
            this.frameIndex = 0;
        } else {
            this.frameIndex++;
        }
        setTimeout(()=>{this.loopFrames(speed)}, speed);
    }


    getAnimationLength() {//define animation frame lengths
        if (this.src.indexOf("walkD") != -1)
        {
            return 9;
        }
        if (this.src.indexOf("walkL") != -1)
        {
            return 9;
        }
        if (this.src.indexOf("walkR") != -1)
        {
            return 9;
        }
        if (this.src.indexOf("walkU") != -1)
        {
            return 9;
        }
    }

    initImg() { //load all subimages

        this.imgs = new Array(this.getAnimationLength()+1)

        for (let i=0;i<=this.getAnimationLength();i++)
        {
            let img = new Image();
            img.useMap = this.src+i+".png";
            img.setAttribute('src', img.useMap);
            this.imgs[i] = img;
        }
    }


}