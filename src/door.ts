class Door {

    protected locked : boolean = false;
    protected room : Room;
    protected direction : number;
    protected itemRequirement = 1;//default req is a standard key
    protected isExit:boolean;

    constructor(room:Room) {
        this.room = room;
    }

    public isLocked() {
        return this.locked;
    }

    public setLocked(state:boolean) {
        this.locked = state;
    }

    public setItemRequirement(itemId:number) {
        this.itemRequirement = itemId;
    }

    public getItemRequirement() {
        return this.itemRequirement;
    }

    public setDirection(n:number) {
        this.direction = n;
    }

    public getDirection() {
        return this.direction;
    }

    public getRoom() {
        return this.room;
    }

    public getIsExit() {
        return this.isExit;
    }

    public setIsExit(b:boolean) {
        this.isExit = b;
    } 

}